# Deploy flask app lên fly.io - free - cần credit-card

Theo <https://fly.io/docs/languages-and-frameworks/python/#install-flyctl-and-login>

### Cài flyctl

<https://fly.io/docs/hands-on/install-flyctl/>

Trên Linux/MacOS gõ: `curl -L https://fly.io/install.sh | sh`

Dấu $ ở đầu dòng đánh dấu đó là câu lệnh, không gõ dấu $ đầu tiên này khi gõ lệnh.

### Đăng ký

`$HOME/.fly/bin/flyctl auth signup`

đăng ký email/password, nhập thẻ credit card. Thẻ này chỉ dùng để phía fly.io
có thể trừ tiền nếu dùng quá nhiều, còn bình thường thì free hàng tháng.

```
$ $HOME/.fly/bin/flyctl launch
Creating app in $HOME/Desktop/flaskfly
Scanning source code
Detected a Python app
Using the following build configuration:
	Builder: paketobuildpacks/builder:base
? Choose an app name (leave blank to generate one):
automatically selected personal organization: HVN
? Choose a region for deployment: Singapore, Singapore (sin)
Created app red-dream-6289 in organization personal
Wrote config file fly.toml
? Would you like to set up a Postgresql database now? No
? Would you like to set up an Upstash Redis database now? No
We have generated a simple Procfile for you. Modify it to fit your needs and run "fly deploy" to deploy your application.

```

### sửa file Procfile thành nội dung như sau
```
web: gunicorn app:app
```

### deploy

```
$ $HOME/.fly/bin/flyctl deploy                                                                                   [0]
==> Verifying app config
--> Verified app config
==> Building image
Remote builder fly-builder-summer-pine-5400 ready
==> Building image with Buildpacks
--> docker host: 20.10.12 linux x86_64
base: Pulling from paketobuildpacks/builder
Digest: sha256:dbc641980f88a7f1f8936302d099bbc45e50d35770b00a9379c5e66e34e7d18a
Status: Image is up to date for paketobuildpacks/builder:base
base-cnb: Pulling from paketobuildpacks/run
Digest: sha256:d7f1f967dfd7c8150e30fc039a5edbd7e5650b75410774a2153d6f3b47c2de76
Status: Image is up to date for paketobuildpacks/run:base-cnb
===> DETECTING
6 of 9 buildpacks participating
paketo-buildpacks/ca-certificates 3.4.0
paketo-buildpacks/cpython         1.7.2
paketo-buildpacks/pip             0.16.1
paketo-buildpacks/pip-install     0.5.7
paketo-buildpacks/python-start    0.14.0
paketo-buildpacks/procfile        5.4.0
===> ANALYZING
Restoring metadata for "paketo-buildpacks/ca-certificates:helper" from app image
Restoring metadata for "paketo-buildpacks/cpython:cpython" from app image
Restoring metadata for "paketo-buildpacks/pip:pip" from cache
Restoring metadata for "paketo-buildpacks/pip-install:packages" from app image
Restoring metadata for "paketo-buildpacks/pip-install:cache" from cache
===> RESTORING
Restoring data for "paketo-buildpacks/cpython:cpython" from cache
Restoring data for "paketo-buildpacks/pip:pip" from cache
Restoring data for "paketo-buildpacks/pip-install:cache" from cache
Restoring data for "paketo-buildpacks/pip-install:packages" from cache
===> BUILDING

Paketo Buildpack for CA Certificates 3.4.0
  https://github.com/paketo-buildpacks/ca-certificates
  Launch Helper: Reusing cached layer
Warning: BOM table is deprecated in this buildpack api version, though it remains supported for backwards compatibility. Buildpack authors should write BOM information to <layer>.sbom.<ext>, launch.sbom.<ext>, or build.sbom.<ext>.
Paketo Buildpack for CPython 1.7.2
  Resolving CPython version
    Candidate version sources (in priority order):
                -> ""
      <unknown> -> ""

    Selected CPython version (using ): 3.10.8

  Reusing cached layer /layers/paketo-buildpacks_cpython/cpython

Warning: BOM table is deprecated in this buildpack api version, though it remains supported for backwards compatibility. Buildpack authors should write BOM information to <layer>.sbom.<ext>, launch.sbom.<ext>, or build.sbom.<ext>.
Warning: BOM table is deprecated in this buildpack api version, though it remains supported for backwards compatibility. Buildpack authors should write BOM information to <layer>.sbom.<ext>, launch.sbom.<ext>, or build.sbom.<ext>.
Warning: BOM table is deprecated in this buildpack api version, though it remains supported for backwards compatibility. Buildpack authors should write BOM information to <layer>.sbom.<ext>, launch.sbom.<ext>, or build.sbom.<ext>.
Paketo Buildpack for Pip 0.16.1
  Resolving Pip version
    Candidate version sources (in priority order):
      <unknown> -> ""

    Selected Pip version (using <unknown>): 22.3.1

  Reusing cached layer /layers/paketo-buildpacks_pip/pip
Warning: BOM table is deprecated in this buildpack api version, though it remains supported for backwards compatibility. Buildpack authors should write BOM information to <layer>.sbom.<ext>, launch.sbom.<ext>, or build.sbom.<ext>.
Warning: BOM table is deprecated in this buildpack api version, though it remains supported for backwards compatibility. Buildpack authors should write BOM information to <layer>.sbom.<ext>, launch.sbom.<ext>, or build.sbom.<ext>.
Paketo Buildpack for Pip Install 0.5.7
  Executing build process
    Running 'pip install --requirement requirements.txt --exists-action=w --cache-dir=/layers/paketo-buildpacks_pip-install/cache --compile --user --disable-pip-version-check'
      Completed in 2.423s

  Generating SBOM for /layers/paketo-buildpacks_pip-install/packages
      Completed in 2ms

  Configuring build environment
    PYTHONPATH -> "/layers/paketo-buildpacks_pip-install/packages/lib/python3.10/site-packages:$PYTHONPATH"

  Configuring launch environment
    PYTHONPATH -> "/layers/paketo-buildpacks_pip-install/packages/lib/python3.10/site-packages:$PYTHONPATH"

Paketo Buildpack for Python Start 0.14.0
  Assigning launch processes:
    web (default): python


Paketo Buildpack for Procfile 5.4.0
  https://github.com/paketo-buildpacks/procfile
  Process types:
    web: gunicorn app:app
===> EXPORTING
Reusing layer 'paketo-buildpacks/ca-certificates:helper'
Reusing layer 'paketo-buildpacks/cpython:cpython'
Adding layer 'paketo-buildpacks/pip-install:packages'
Adding 1/1 app layer(s)
Reusing layer 'launcher'
Reusing layer 'config'
Reusing layer 'process-types'
Adding label 'io.buildpacks.lifecycle.metadata'
Adding label 'io.buildpacks.build.metadata'
Adding label 'io.buildpacks.project.metadata'
Setting default process type 'web'
Saving registry.fly.io/red-dream-6289:cache...
*** Images (c07da6611e69):
      registry.fly.io/red-dream-6289:cache
      registry.fly.io/red-dream-6289:deployment-01GJWPBNCPS51GG597ZPJ0Y3ZC
Reusing cache layer 'paketo-buildpacks/cpython:cpython'
Reusing cache layer 'paketo-buildpacks/pip:pip'
Adding cache layer 'paketo-buildpacks/pip-install:cache'
Adding cache layer 'paketo-buildpacks/pip-install:packages'
--> Building image done
==> Pushing image to fly
The push refers to repository [registry.fly.io/red-dream-6289]
83d85471d9f8: Layer already exists
12fe2a51b197: Layer already exists
858993968edd: Layer already exists
a8097334847b: Pushed
ddf9bcf7eefb: Pushed
d6293bae7bfb: Layer already exists
c23057e83cef: Layer already exists
281c9720605c: Layer already exists
2009ac93cdeb: Layer already exists
5a6f371cf5c2: Layer already exists
956411b2bc48: Layer already exists
bfeecddfe4a8: Layer already exists
a0b64f71f216: Layer already exists
69f57fbceb1b: Layer already exists
deployment-01GJWPBNCPS51GG597ZPJ0Y3ZC: digest: sha256:94d0955fce37c8fd38ec08d23f2eaaa5a4d636a047699bab9d42e9e6e85e0aba size: 3241
--> Pushing image done
image: registry.fly.io/red-dream-6289:deployment-01GJWPBNCPS51GG597ZPJ0Y3ZC
image size: 306 MB
==> Creating release
--> release v1 created

--> You can detach the terminal anytime without stopping the deployment
==> Monitoring deployment

 1 desired, 1 placed, 0 healthy, 0 unhealthy
 1 desired, 1 placed, 1 healthy, 0 unhealthy [health checks: 1 total, 1 passing]
--> v1 deployed successfully

$ $HOME/.fly/bin/flyctl status
App
  Name     = red-dream-6289
  Owner    = personal
  Version  = 1
  Status   = running
  Hostname = red-dream-6289.fly.dev
  Platform = nomad

Deployment Status
  ID          = bf6bbb54-548a-596f-7d54-43fad41367dc
  Version     = v1
  Status      = successful
  Description = Deployment completed successfully
  Instances   = 1 desired, 1 placed, 1 healthy, 0 unhealthy

Instances
ID      	PROCESS	VERSION	REGION	DESIRED	STATUS 	HEALTH CHECKS     	RESTARTS	CREATED
9241a9bd	app    	1      	sin   	run    	running	1 total, 1 passing	0       	10m13s ago
```

Truy cập <https://red-dream-6289.fly.dev> xem kết quả.

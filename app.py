from flask import Flask

app = Flask("myapp")


def chao():
    return "Hello, Pymier"
   
def bye():
    return "bye bye bey"

def hat():
    return "bai hat chìm trong nỗi nhớ"
     
app.add_url_rule("/", "chao", chao)
app.add_url_rule("/bye", "bye", bye)
app.add_url_rule("/sing", "hat", hat)

@app.route("/football") # dùng decorator 
def xembongda():
    return "Sut, vao!!!"

@app.route("/binhphuong/<int:n>")
def square(n):
    return str(n * n)

@app.route("/hello/<string:name>/<int:age>")
def hello(name, age):
    return f"Xin chao ban {name.upper()} {age} tuoi"

students = [
    {'name': "Duong", "age": 20},
    {"name": "Minh", "age": 23},
]
from flask import render_template
@app.route("/class")
def myclass():
    return render_template("list.html", students=students)

    
    
from flask import request
import json
## Web JSON RESTful API
@app.route("/api/v1/students", methods=["GET", "POST"])
def list_students():
    if request.method == "GET":
        return students
    elif request.method == "POST":
        print("Method", request.method, request.data)
        s = request.data.decode("utf-8")
        students.append(json.loads(s))
        return {"status": "OK"}

@app.route("/api/v1/add_student", methods=["POST"])
def add_student():
    # NOT RESTful
    # TODO
    return students


if __name__ == "__main__":
    app.run(debug=True)
